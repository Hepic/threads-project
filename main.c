#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include "queue.h"
#include "help_functions.h"

#define num_of_queues 4

struct timespec init_time;
double total_wait_time;
int total_customers;
int waiting_customers;
Queue queues[num_of_queues];
pthread_mutex_t mtx;
pthread_cond_t wake_clerk;


int main(int argc, char *argv[])
{
    clock_gettime(CLOCK_MONOTONIC, &init_time);

    if(argc < 2)
        print_exit("File Required\n");
    
    const char *name_file = argv[1];
    FILE *input = fopen(name_file, "r");
    
    int N_cust, id, arv, srv, N_clerk = 2;
    int i, err, status, clerk_id[2] = {0, 1};
    Customer *customers;
    pthread_t *cust_thr, clerk_thr[2];
	
	for(i=0; i<num_of_queues; i++)
		initialize_queue(&queues[i]);
	
    pthread_mutex_init(&mtx, 0);
	pthread_cond_init(&wake_clerk, 0);

    fscanf(input, "%d", &N_cust);	
    
    if((cust_thr = (pthread_t*)malloc(sizeof(pthread_t) * N_cust)) == NULL)
        print_exit("malloc");
    
    if((customers = (CustomerPtr)malloc(sizeof(Customer) * N_cust)) == NULL)
        print_exit("malloc");
    
    int pos = 0;

    for(i=0; i<N_cust; ++i)
    {
        fscanf(input, "%d:%d,%d", &id, &arv, &srv);
        
        if(id < 0 || arv < 0 || srv < 0)
            continue;
            
        struct timespec temp;
        set_customer(customers+pos, id, arv, srv, temp);
        ++pos;
    }

    N_cust = pos;
	total_customers = N_cust;
    
    for(i=0; i<N_clerk; ++i) // Create threads(Clerks)
    {
        if(err = pthread_create(clerk_thr+i, NULL, clerk_procedure, (int*)&clerk_id[i]))
            print_exit("pthread_create");
    }
    
    for(i=0; i<N_cust; ++i) // Create threads(Customers)
    {
        if(err = pthread_create(cust_thr+i, NULL, customer_procedure, (void*)&customers[i]))
            print_exit("pthread_create");
    }
    
    for(i=0; i<N_clerk; ++i)
    {
        if(err = pthread_join(clerk_thr[i], NULL))
            print_exit("pthread_join");
    }
    
    for(i=0; i<N_cust; ++i)
    {
        if(err = pthread_join(cust_thr[i], NULL))
            print_exit("pthread_join");
    }

    printf("The average waiting time for all customers in the system is: %.2f seconds. \n", (double)total_wait_time / (double)N_cust);
	
    pthread_cond_destroy(&wake_clerk);
	pthread_mutex_destroy(&mtx);
    
    free(cust_thr);
    free(customers);    

    fclose(input);
    return 0;
}
