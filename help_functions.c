#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>
#include "help_functions.h"
#include "customer.h"

void print_exit(const char *msg)
{
    fprintf(stderr, "%s", msg);
    exit(EXIT_FAILURE);
}

void *customer_procedure(void *args)
{
    Customer cust = *((Customer*)args);
	int i, min_size, min_queue;
	
    usleep(100000 * cust.arrival);
	printf("A customer arrives: customer ID: %d\n", cust.id);
	
	pthread_mutex_lock(&mtx);
	
	min_size = queue_size(queues[0]);
	min_queue = 0;

	for(i=1; i<num_of_queues; i++)
	{
		if(queue_size(queues[i]) < min_size)
		{
			min_size = queue_size(queues[i]);
			min_queue = i;
		}
	}
	
	printf("A customer enters a queue: queue ID: %d, length of the queue: %d.\n", min_queue, min_size);
    set_time_arrival(&cust);
    insert_queue(&queues[min_queue], cust);
    ++waiting_customers; // increase customer who wait to be served by one
	
	pthread_mutex_unlock(&mtx);
	pthread_cond_signal(&wake_clerk);

    pthread_exit(NULL);
}

void *clerk_procedure(void *args)
{
	int id = *((int*)args), i, max_size, max_queue;
	CustomerPtr customer_picked = NULL;
	
    while(1)
	{
		pthread_mutex_lock(&mtx);
	
		while(waiting_customers <= 0) // if there is not customer to be server, clerk waits 
		{
            pthread_cond_wait(&wake_clerk, &mtx);

		    if(total_customers == 0)
		        break;
        }

        if(total_customers == 0) // if all customer are served then clerk can stop
		    break;
        
        max_size = 0;
		
        for(i=0; i<num_of_queues; i++)
		{
			if(queue_size(queues[i]) > max_size)
			{
				max_size = queue_size(queues[i]);
				max_queue = i;
			}
		}
	    
		customer_picked = create_customer(*front_queue(queues[max_queue])); // pick the first customer from the largest queue
		pop_queue(&queues[max_queue]);
		
		--waiting_customers;
		--total_customers;
		
		pthread_mutex_unlock(&mtx);
	    
        struct timespec curr_time;
        clock_gettime(CLOCK_MONOTONIC, &curr_time);
        
        double diff_time = curr_time.tv_sec - init_time.tv_sec;
        diff_time += (curr_time.tv_nsec - init_time.tv_nsec) / 1000000000.0;
        
        double diff_wait = curr_time.tv_sec - customer_picked->time_arrival.tv_sec;
        diff_wait += (curr_time.tv_nsec - customer_picked->time_arrival.tv_nsec) / 1000000000.0;
        total_wait_time += diff_wait;

		printf("A clerk starts serving a customer: start time %.2f, the customer ID: %d, the clerk ID: %d.\n", diff_time * 10, customer_picked->id, id);
        usleep(100000 * customer_picked->service);
	    
        clock_gettime(CLOCK_MONOTONIC, &curr_time);
        
        diff_time = curr_time.tv_sec - init_time.tv_sec;
        diff_time += (curr_time.tv_nsec - init_time.tv_nsec) / 1000000000.0;

        printf("A clerk finishes serving a customer: end time %.2f, the customer ID: %d, the clerk ID: %d.\n", diff_time * 10, customer_picked->id, id);
	    free(customer_picked);

		if(total_customers == 0)
		{
            pthread_cond_broadcast(&wake_clerk);
            break;
	    }
    }

    pthread_exit(NULL);
}
