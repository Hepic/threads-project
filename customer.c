#include "customer.h"

CustomerPtr create_customer(Customer cust)
{
    CustomerPtr customer = (CustomerPtr)malloc(sizeof(Customer));
    set_customer(customer, cust.id, cust.arrival, cust.service, cust.time_arrival);
    
    return customer;
}

void set_customer(CustomerPtr customer, int id, int arrival, int service, struct timespec time_arrival)
{
    customer->id = id;
    customer->arrival = arrival;
    customer->service = service;
    customer->time_arrival = time_arrival;
}

void print_customer(Customer customer)
{
    printf("Id: %d, Arrival: %d, ", customer.id, customer.arrival);
    printf("Service %d\n", customer.service);
}

void set_time_arrival(CustomerPtr customer)
{
    struct timespec curr_time;
    clock_gettime(CLOCK_MONOTONIC, &curr_time);
    
    customer->time_arrival = curr_time;
}
