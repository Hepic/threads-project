#ifndef QUEUE_H
#define QUEUE_H

#include <stdio.h>
#include <stdlib.h>
#include "customer.h"

typedef struct Queue* QueuePtr;
typedef struct Node* NodePtr;

typedef struct Node
{
    CustomerPtr customer;
    NodePtr next;

}Node;

typedef struct Queue
{ 
    NodePtr head, tail;
	int queue_size;
}Queue;

NodePtr create_node(Customer);
void initialize_queue(QueuePtr);
void insert_queue(QueuePtr, Customer);
void pop_queue(QueuePtr);
CustomerPtr front_queue(Queue);
int is_empty(Queue);
int queue_size(Queue);

#endif
