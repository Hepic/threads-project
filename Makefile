CC = gcc
FLAGS = -g -c
OUT = run
OBJS = main.o customer.o help_functions.o queue.o

run: $(OBJS)
	$(CC) -g $(OBJS) -o $(OUT) -lpthread 

%.o: %.c %.h
	$(CC) $(FLAGS) $< -o $@

clean:
	rm -rf $(OBJS) $(OUT) 
