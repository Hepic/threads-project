#include "queue.h"

NodePtr create_node(Customer cust)
{
    NodePtr new_node = (NodePtr)malloc(sizeof(Node));
    new_node->customer = create_customer(cust); 
    new_node->next = NULL;

    return new_node;
}

void initialize_queue(QueuePtr que)
{
    que->head = que->tail = NULL;
	que->queue_size = 0;
}

void insert_queue(QueuePtr que, Customer cust)
{
    NodePtr new_node = create_node(cust);
	que->queue_size++;
	
    if(que->head == NULL)
    {
        que->head = que->tail = new_node;
        return;
    }
    
    que->tail->next = new_node;
    que->tail = new_node;
}

void pop_queue(QueuePtr que)
{
    NodePtr tmp_next = que->head->next;

    free(que->head->customer);
    free(que->head);
    
    que->head = tmp_next;
	que->queue_size--;
}

CustomerPtr front_queue(Queue que)
{
    return que.head->customer;
}

int is_empty(Queue que)
{
    return que.head == NULL;
}

int queue_size(Queue que)
{
	return que.queue_size;
}
