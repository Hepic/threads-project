#ifndef HELP_FUNCTIONS_H
#define HELP_FUNCTIONS_H

#include <pthread.h>
#include <time.h>
#include "queue.h"

#define num_of_queues 4

extern struct timespec init_time;
extern double total_wait_time;
extern int total_customers;
extern int waiting_customers;
extern pthread_mutex_t mtx;
extern pthread_cond_t wake_clerk;
extern Queue queues[num_of_queues];

void perror_exit(const char*);
void *customer_procedure(void*);
void *clerk_procedure(void*);

#endif
