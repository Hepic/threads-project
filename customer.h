#ifndef CUSTOMER_H
#define CUSTOMER_H

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

typedef struct Customer* CustomerPtr;

typedef struct Customer
{
    int id;
    int arrival, service;
    struct timespec time_arrival;

}Customer;

CustomerPtr create_customer(Customer);
void set_customer(CustomerPtr, int, int, int, struct timespec);
void print_customer(Customer);

#endif
